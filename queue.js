let collection = [];

// Write the queue functions below.
function print(){
	return collection;
}

function enqueue(element){
	collection[collection.length] = element
	return collection
}

function dequeue(element){
	const remainingElements = [];
	for (i = 0 ; i < collection.length-1 ; i++) {
		remainingElements[i] = collection[i+1];
	}
	collection = remainingElements;
	return collection
}

function front(){
	return(collection[0])
}

function size(){
	return(collection.length)
}

function isEmpty(){
	return(collection.length == 0 ? true : false)
}



module.exports = {
	collection,
	print,
	enqueue,
	dequeue,
	front,
	size,
	isEmpty
};